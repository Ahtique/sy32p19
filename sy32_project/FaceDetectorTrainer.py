"""
Face detection project, SY32, P19
Training part of the project
"""

from random import shuffle
from typing import List, Tuple, Callable

import numpy as np
import pandas as pd
from sklearn.base import ClassifierMixin
from sklearn.model_selection import KFold

from api import parse_image, load_images, load_labels


class FaceDetectorTrainer:
    """
    Helper class used to facilitate processing data and training a classifier
    """

    def __init__(self,
                 windows_size: Tuple[int, int] = (120, 90),
                 windows_step: int = 30,
                 resizing_factors: List[float] = None,
                 transform_function: Callable[[np.ndarray], np.ndarray] = None,
                 classifier: ClassifierMixin = None,
                 images: List = None,
                 labels: pd.DataFrame = None
                 ) -> None:
        """
        Constructor. All parameters are optional

        :param windows_size: sliding window size as a tuple (height, width)
        :param windows_step: sliding windows step
        :param resizing_factors: factors used to resize images during the sliding window process
        :param transform_function: function used to transform the output of the sliding window phase
            example: lambda x: hog(x)
        :param classifier: the classifier to train. Can be specified later.
        :param images: List of the training images
        :param labels: list of labels
        """
        super().__init__()
        self.boxes = []
        self.window_size = windows_size
        self.windows_step = windows_step
        self.resizing_factors = resizing_factors
        self.transform_function = transform_function
        self.classifier = classifier
        self.images = images
        self.labels = labels

    def prepare_training_vectors(self,
                                 images: List,
                                 labels: pd.DataFrame,
                                 ) -> None:
        """
        Prepare training data to be fed to a classifier chopping it into parts
        (sliding window style) and transform them if needed.
        Result is sored in th trainer's memory.

        :param images: a list of images used to build our x_train vector
        :param labels: labels in a pandas DF (k,i,j,h,l) identifying the position
            of faces in images k
        :return: None, set the internal state of the trainer
        """
        # reset the data contained in the trainer
        self.boxes = []
        for index, image in enumerate(images):
            # faces boxes in the image
            faces = labels[labels.k == index]
            # get the boxes containing windows and classes
            tmp_boxes = parse_image(img=image,
                                    list_positives=faces,
                                    window_sizes=self.window_size,
                                    windows_step=self.windows_step,
                                    img_resizing_factors=self.resizing_factors)
            # add image number for convenience purpose to the boxes
            for b in tmp_boxes:
                b.img_k = index
            # add the new boxes to the trainer's memory
            self.boxes += tmp_boxes
        # if a transformation is to be applied to every part of image
        if self.transform_function is not None:
            for b in self.boxes:
                b.image = self.transform_function(b.image)

    def train_classifier(self, test_proportion: float = .1) -> float:
        """
        Split the training vectors according to test_proportion with

        test_proportion part of data reserved for testing purpose.
        :param test_proportion: the proportion of data which will be used to
            test the trained classifier
        :return: the trained classifier performance on the test vector
        """
        self.boxes = shuffle(self.boxes)
        x = [e.image for e in self.boxes]
        y = [e.class_val for e in self.boxes]
        n_test = int(len(x) * test_proportion)
        x_test, y_test = (x[:n_test], y[:n_test])
        x_train, y_train = (x[n_test + 1:], y[n_test + 1:])
        self.classifier.fit(x_train, y_train)
        return self.classifier.score(x_test, y_test)

    def train_classifier_k_fold(self, folds: int = 5) -> float:
        """
        train the memorized classifier using a cross validation method

        :param folds: number of slices into the training vectors
        :return: the score of the trained classifier
        """
        self.boxes = shuffle(self.boxes)
        x = [e.image for e in self.boxes]
        y = [e.class_val for e in self.boxes]
        kf = KFold(n_splits=folds)
        kf.get_n_splits(x)
        scores = [self.classifier
                      .fit(x[train], y[train])
                      .score(x[test], y[test]) for train, test in kf.split(x)]
        avg_score = np.mean(scores)
        return float(avg_score)

    def images_from_files(self, path: str) -> int:
        """
        Load images from a folder identified by its path

        :param path: path to folder
        :return: number of image obtained
        """
        self.images = load_images(path)
        return len(self.images)

    def labels_from_file(self, path: str) -> None:
        """
        Load labels describing faces in images from a file

        :param path: path to file
        :return: nthing, update the trainer's memory
        """
        self.labels = load_labels(path, scores=True)
