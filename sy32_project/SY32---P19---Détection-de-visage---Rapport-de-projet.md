---
title: SY32 - P19 - Détection de visage - Rapport de projet
author: William Veal Phan, GI04 (Groupe 17)
---

# Objectif

Ce projet a pour objectif la production d'un détecteur de visage codé en Python à l'aide, principalement, des bibliothèques Scikit. 

Plus spécifiquement, deux scripts Python sont attendus:

- train.py : il doit permettre l'entrainement d'un détecteur de visage à partir de données d'entrée et le sauvegarder pour pouvoir être réutilisé ultérieurement
- test.py : il doit utiliser le détecteur précédemment produit afin de détecter les visages dans un ensemble de test passé en entrée. Il doit enregistrer les positions de ces visages avec les scores de détection dans un fichier .txt qui sera téléchargés vers le site permettant l'évaluation de la prédiction

# Fonctionnement du détecteur de visages

Une approche par fenêtre glissante a été choisie pour ce projet. Ainsi, une image sera décomposée en une multitude de fenêtres de même taille, et à différentes échelles. Un classifieur de visage est ensuite utilisé pour déterminer si un visage est présent dans chaque fenêtre.

Entrainer un détecteur de visage sur un ensemble d'image revient ainsi à entrainer un classifieur de visage sur un ensemble de fenêtres. Et détecter des visages dans une image revient alors à découper cette image en fenêtres et à utiliser un classifieur entraîné sur chaque fenêtre obtenue. 

Cela implique de diviser ce projet en deux parties. Ainsi, il s'agira dans un premier temps d'implémenter la logique du détecteur indépendamment de la forme du classifieur, puis dans un second temps de déterminer les paramètres du classifieur expérimentalement.

# Implémentation du détecteur de visages

Dans une optique de lisibilité et de ré-usabilité, j'ai décidé de commencer par construire une bibliothèque contenant toute la logique du détecteur de visage.

## Bibliothèque 

### API: lecture, écriture, classes partagées

Le premier module créé est api.py (plus tard dupliqué en api_parallel.py). Ce module regroupe les fonctions et classes communes aux deux suivants, ainsi que les fonctions permettant de charger les données. En particulier:

- la classe Box identifie une partie rectangulaire d'une image;
- la fonction parse_image effectue un balayage par fenêtre glissante d'une image et produit donc une liste de Box contenant les informations relatives à la partie d'image associée
- load_images et load_labels encapsulent le processus de chargement des données. Les labels sont stockés sous forme de pandas.DataFrame, cette classe permettant de manipuler ces données plus aisément et de façon plus transparente que de simple np.ndarray

La version parallel de api.py effectue les même tâches mais en utilisant sklearn.externals.joblib afin de paralléliser certaines opérations. L'usage de cette version est recommandé.

### FaceDetectorTrainer: encapsulation du processus d'apprentissage

La deuxième étape a ensuite été de construire une classe permettant l'apprentissage d'un classifieur de visages. Cette classe est le FaceDetectorTrainer(FDT). Un FDT prendra pour paramètres:

- les paramètres de la fenêtres glissante, c'est à dire ses tailles, son pas et les facteurs utilisés pour le redimensionnement de l'mage
- une transformation à appliquer aux images produites par la fenêtre glissante, telle que la conversion en histogrammes des gradients orientés, descripteurs DAISY.... 
- le classifieur à entraîner, qui peut être un classifieur "simple" tel qu'un LinearSVC, ou, et c'est le choix recommandé, un classifieur ensembliste tel qu'un RandomForestClassifier ou un AdaBoostClassifier. En effet, le FDT ne réutilisera pas les données mal classées de lui même afin d'améliorer les performances du classifieur. Il est donc judicieux d'utiliser un classifieur qui le fera.

Une fois un FDT instancié, il faut utiliser, dans l'ordre:

- prepare_training_vectors qui va effectuer les prétraitements nécessaires sur les images et les labels qu'on lui fournit: le FDT va alors appliquer le parse_image de l'API sur chaque image, déterminer si cette image contient un visage en fonction des labels et appliquer le la transformation si celle ci est spécifiée
- train_classifier ou train_classifier_k_fold permettent d'effectuer l'apprentissage du classifieur à partir des données traitées mémorisées par le FDT. La version k_fold effectue l'apprentissage par validation croisée, bien plus couteux que la version basique qui isolera une partie des données d'entrainement pour l'évaluation des perormances
- dump_classifier qui permet de le sérialiser le classifieur dans un fichier afin de pouvoir le réutiliser plus tard sans repasser par l'ensemble du processus d'apprentissage

### FaceDetector : encapsulation du processus de détection

Une fois un classifieur entrainé, il peut être chargé en utilisant sklearn.externals.joblib.load qui permet de le désérialiser. Un FaceDetector (FD) peut être initialisé à partir des mêmes paramètres que le FDT ou à partir d'un FDT directement afin de s'assurer de l'uniformité de la forme des données entrainement et des données de prédiction. 

Un paramètre a toutefois été ajouté afin de pouvoir utiliser convenablement la bibliothèque Scikit-learn : predict_function permet de préciser si le classifieur prédit à partir de decision_function ou predict_proba. 

Un FD une fois initialisé avec un classifieur détecte les visages dans une liste d'mages à l'aide de la méthode find_faces. Le même traitement que pendant l'apprentissage est appliqué aux images et le classifieur fourni identifie ensuite les visages. L'élimination des fenêtres chevauchantes se fait en gardant la réponse maximale parmi les boites chevauchantes. Deux fenêtres sont chevauchante si l'intersection représente plus de 50% de l'aire totale.

Enfin, write_last_pred écrit la dernière prédiction effectuée par le détecteur de visage dans un fichier en csv sans nom de colonne ni numéro de ligne, avec " " comme séparateur, et les colonnes:

- k : le numéro de l'image
- (i,j) : positon du coin supérieur gauche en ligne et colonne
- (h,l) : hauteur et largeur de la fenêtre
- s : le score de détection

Par défaut, le détecteur gardera et écrira toutes les prédictions dont le score est supérieur à 0.

Des méthodes permettant de calculer ses performances ont aussi été ajoutées, permettant notamment de calculer sa courbe précision rappel et l'air sous la courbe. Cet indicateur est plus pertinent que la courbe ROC : en effet, le FDT garde l'ensemble des fenêtres pour constituer l'échantillon d'apprentissage. Ces fonctions n'ont toutefois pas été testées en profondeur.

## Utilisation

L'entrainement et l'utilisation d'un détecteur sont illustrés dans les scripts fournis avec ce document. 

### train.py

1. Définition des paramètres : classifieur, taille des fenêtres, pas, facteurs de redimensionnement, transformation à appliquer aux images, répertoire des images, fichier des labels, et optionnellement le fichier où sauvegarder le classifieur obtenu;
2. Instanciation d'un FaceDectectorTrainer avec ces paramètres;
3. Chargement des données: load_images et load_labels de l'API
4. Préparation des vecteurs d'apprentissage: FaceDetectorTrainer.prepare_training_vectors;
5. Apprentissage du classifieur: FaceDetectorTrainer.train_classifier;
6. Sérialisation du classifieur entraîné si nécessaire: FaceDetectorTrainer.dump_classifier.

### test.py

1. Définition des paramètres : classifieur, fonction de prédiction, taille des fenêtres, pas, facteurs de redimensionnement, transformation à appliquer aux images, répertoire des images, fichier où inscrire les prédictions;
2. Instanciation d'un FaceDetector avec ces paramètres
3. Chargement des données: load_images
4. Prédiction : FaceDetector.find_faces
5. Ecriture des résultats : FaceDetector.write_last_pred

# Résultats

## Choix des paramètres

Les versions des scripts fournies avec ce rapport prennent en paramètres:

- une fenêtres de taille (160, 110) qui correspond a la taille moyenne des fenêtres dans le fichier label fourni avec les données d'entrainement;
- un pas de 20 relativement grand par rapport à la fenêtre mais choisi pour des raisons de performance;
- aucun facteur de dimensionnement;
- une transformation par histogramme des gradients orientés : on suppose que des visages ont une distribution de gradients assez caractéristique, notamment la forme ovoïde, les yeux et la bouche;
- un classifieur en forêt aléatoire

exécution de train.py donne :

```
Chargement des donnees
Chargement termine en  3.8833611011505127
Préparation des vecteurs d'apprentissage
Transformed  104647 / 104648
Prep terminee en  7534.615961074829
Apprentisage
[Parallel(n_jobs=-1)]: Using backend ThreadingBackend with 8 concurrent workers.
[Parallel(n_jobs=-1)]: Done  34 tasks      | elapsed:  3.8min
[Parallel(n_jobs=-1)]: Done 100 out of 100 | elapsed:  9.5min finished
[Parallel(n_jobs=8)]: Using backend ThreadingBackend with 8 concurrent workers.
[Parallel(n_jobs=8)]: Done  34 tasks      | elapsed:    1.3s
[Parallel(n_jobs=8)]: Done 100 out of 100 | elapsed:    3.4s finished
[Parallel(n_jobs=-1)]: Using backend ThreadingBackend with 8 concurrent workers.
[Parallel(n_jobs=-1)]: Done  34 tasks      | elapsed:  3.5min
[Parallel(n_jobs=-1)]: Done 100 out of 100 | elapsed:  9.0min finished
[Parallel(n_jobs=8)]: Using backend ThreadingBackend with 8 concurrent workers.
[Parallel(n_jobs=8)]: Done  34 tasks      | elapsed:    1.3s
[Parallel(n_jobs=8)]: Done 100 out of 100 | elapsed:    3.9s finished
[Parallel(n_jobs=-1)]: Using backend ThreadingBackend with 8 concurrent workers.
[Parallel(n_jobs=-1)]: Done  34 tasks      | elapsed:  3.4min
[Parallel(n_jobs=-1)]: Done 100 out of 100 | elapsed:  8.7min finished
[Parallel(n_jobs=8)]: Using backend ThreadingBackend with 8 concurrent workers.
[Parallel(n_jobs=8)]: Done  34 tasks      | elapsed:    1.2s
[Parallel(n_jobs=8)]: Done 100 out of 100 | elapsed:    3.2s finished
[Parallel(n_jobs=-1)]: Using backend ThreadingBackend with 8 concurrent workers.
[Parallel(n_jobs=-1)]: Done  34 tasks      | elapsed:  3.4min
[Parallel(n_jobs=-1)]: Done 100 out of 100 | elapsed:  8.9min finished
[Parallel(n_jobs=8)]: Using backend ThreadingBackend with 8 concurrent workers.
[Parallel(n_jobs=8)]: Done  34 tasks      | elapsed:    1.2s
[Parallel(n_jobs=8)]: Done 100 out of 100 | elapsed:    3.2s finished
[Parallel(n_jobs=-1)]: Using backend ThreadingBackend with 8 concurrent workers.
[Parallel(n_jobs=-1)]: Done  34 tasks      | elapsed:  3.3min
[Parallel(n_jobs=-1)]: Done 100 out of 100 | elapsed:  8.7min finished
[Parallel(n_jobs=8)]: Using backend ThreadingBackend with 8 concurrent workers.
[Parallel(n_jobs=8)]: Done  34 tasks      | elapsed:    1.5s
[Parallel(n_jobs=8)]: Done 100 out of 100 | elapsed:    5.3s finished
Apprentissage termine en  3787.531620502472
Err = 0.957457373970928
```

Python utilisait 100% des ressources systèmes pendant la majeure partie de l'apprentissage, soit 100% des 8 cores de mon I7-7700HQ, ainsi que 12 Go de RAM et 37Go de swap.

L'exécution de test.py se fait ensuite en 1208 secondes.

## Problèmes notables et performances

L'implémentation faite dans une optique d'optimisation du temps de calcul. Lors des tests, cela s'est révélé être un problème majeur : en effet, un pas petit de l'ordre de 10 pixels serait nécessaire à l'obtention de bons résultats, mais mon ordinateur se retrouve à cours de mémoire et de swap à ce moment là. 

Et dû à la mise en mémoire virtuelle la vitesse d'apprentissage reste limitée par les débits de lecture et d'écriture. Un classifieur relativement performant prendrais quelques heures en apprentissage, pour pouvoir traiter environ 1 image par seconde. 

J'ai aussi fait des erreurs difficiles à corriger qui dont je ne me suis aperçu qu'arrivé au test sur le serveur (sur lequel j'ai obtenu un malheureux 2% à cause d'une erreur d'indice).

## Pistes d'amélioration

Ne pas sauvegarder des copies des images pour accélérer le temps de calcul serait un bon début, quitte à recalculer les  les transformations à chaque fois. Ce point me semble être ma plus grande erreur d'implémentation. Tous les gains en temps sont handicapés par les limitations matérielles de la machine, surtout par sa quantité de RAM. L'utilisation d'un GPU pour les tâches parallèlisables, et notamment les transformations, pourrait aussi amener à un gain de vitesse. 

Ces réalisation arrivent toutefois trop tard pour que je puisse mettre à jour mon projet en entier. Il est ainsi recommandé d'avoir environ 50 go de RAM + swap pour pouvoir faire tourner le script fourni. 

Enfin, les classifieurs entrainés produisent beaucoup de faux positifs avec beaucoup de certitude (entre 0.9 et 1.0), laissant à penser que c'est bien au niveau de échantillonnage et non du seuil de détection du classifieur que le problème réside.



i