"""
Face detection project, Sy32, P19
Commons objects and functions used in training and testing
by William Veal Phan
"""

from typing import List, Tuple

import numpy as np
import pandas as pd
from skimage import io, transform
from skimage.color import rgb2gray



# ================ Custom objects ===================

class Box:
    """A box is a part of a picture"""

    def __init__(self, i=0, j=0, h=170, w=111, s=0, image=None, k=0):
        self.i = i
        self.j = j
        self.h = h
        self.w = w
        self.s = s
        self.k = k
        self.image = image

    def __repr__(self):
        return "Box: i={}, j={}, h={}, w={}, s={}".format(self.i, self.j,
                                                          self.h, self.w,
                                                          self.s)

    def max_row(self):
        return self.i + self.h

    def max_col(self):
        return self.j + self.w

    def area(self):
        return self.h * self.w

    def overlap(self, other):
        """Determine if this Box is overlapping with another

            :param self: this box
            :param other: another box
            :return a number between 0 and 1, the overlapping proportion
        """
        if min(self.max_row(), other.max_row()) <= max(self.i, other.i):
            return 0
        elif min(self.max_col(), other.max_col()) <= max(self.j, other.j):
            return 0
        else:
            # we know they are overlapping
            x_overlap = min(self.max_col(), other.max_col()) - max(self.j,
                                                                   other.j)
            y_overlap = min(self.max_row(), other.max_row()) - max(self.i,
                                                                   other.i)
            area_overlap = x_overlap * y_overlap
            return area_overlap / (self.area() + other.area() - area_overlap)

    def scale(self, scale_factor):
        """
        Rescale this box by multiplying its fields by a factor

        :param scale_factor: the scale factor used to magnify or downsize the
        box
        :return: this object
        """
        self.i = int(scale_factor * self.i)
        self.j = int(scale_factor * self.j)
        self.h = int(scale_factor * self.h)
        self.w = int(scale_factor * self.w)
        return self

    @classmethod
    def max_box(cls, boxes):
        """
        Compute the box with the max score. Sort and flip.

        Parameters
        ----------
        boxes: List[Box]
            a list of Boxes with a score

        Returns
        -------
            the box with the highest score

        """
        # sort in descending order
        boxes = np.flip(sorted(boxes, key=lambda e: e.s))
        max_box: Box = boxes[0]
        return max_box


# ======================= Utility methods ===========================

def load_images(path: str) -> List:
    """Load images from a folder
        :param path: string path to the folder
        :return a list of images
    """
    images = [rgb2gray(e) for e in io.imread_collection(path + "/*")]
    return images


def load_labels(path: str, scores: bool = False) -> pd.DataFrame:
    """Load labels as a Pandas DataFrame

        :param path: the path the label file. It should be formatted as
            k : image number,
            i, j : box coordinates (row, column)
            h, l : box height and width
            with no column names on the first row and separator = ' '
        :param scores: if True, then the file has another column score
        :return a dataframe containing all the faces identified in the label
        file
    """
    col_names = ['k', 'i', 'j', 'h', 'l']
    if scores:
        col_names.append('s')
    df = pd.read_csv(path, sep=' ', header=None, names=col_names)
    return df


def parse_image(img: any,
                list_positives: List[Box] = None,
                window_sizes: Tuple[int, int] = (60, 40),
                windows_step: int = 10,
                img_resizing_factors: List[float] = None) -> List[Box]:
    """Parse an image in a sliding window style

    :param img: an image from which we need to extract a list of parts
    :param list_positives: list of boxes identifying the position of faces if
    there are some in the picture.
        Default behaviour is None, meaning it wont bother to match anything
        and will just slice the picture
    :param window_sizes: a tuple (height, width) describing the window size
    :param windows_step: step of the slide
    :param img_resizing_factors: specify factors to resize the image and thus
    obtain differently sized Boxes
    :return: List of Boxes, each one containing at least the box's shape and
    position in the image,
        as well as the part of the image it covers and a class (class_val) if
        list_positive is not null
    """
    boxes: List[Box] = []
    (h, w) = window_sizes
    s = windows_step
    factors = [1.0]
    # if we precised we need to resize multiple times
    if img_resizing_factors is not None:
        factors += img_resizing_factors
    for f in factors:
        tmp_boxes = []
        tmp_img = transform.rescale(img, f, anti_aliasing=True)
        # sliding window
        for x in range(0, np.shape(tmp_img)[1] - w, s):
            for y in range(0, np.shape(tmp_img)[0] - h, s):
                # get the window and save the sub picture in parts
                # parts in the list that should be returned if this was a
                # function
                window = tmp_img[x:x + w, y:y + h]
                # check we have a fll window and not a border
                # without that, we might have weird shapes and errors
                if np.shape(window) == (w, h):
                    # generate a boxcla
                    tmp = Box(x, y, h, w, image=window.copy())
                    tmp_boxes.append(tmp)
        # re scale every box but keep the image in the same scale
        tmp_boxes = [e.scale(1 / f) for e in tmp_boxes]
        boxes += tmp_boxes
    # if we are also able to specify the class of each Box's image
    if list_positives is not None:
        for b in boxes:
            tmp_class = -1
            for e in list_positives:
                if b.overlap(e) >= 0.5:
                    tmp_class = 1
                    break
            b.class_val = tmp_class
    return boxes
