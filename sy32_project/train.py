from time import time

from skimage.feature import hog
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier

import api_parallel as api
from FaceDetectorTrainer import FaceDetectorTrainer

# ==================== Parametrage de l'apprentissage ==================

# Classifieur
cls = RandomForestClassifier(n_estimators=100, n_jobs=-1, verbose=1)
# Fenetre glissante
# forme
(hauteur, largeur) = (160, 110)
# pas de glissement
pas = 20
# facteurs de redimensionnement
facteurs = []
# traitement appliqué à chaque fenetree
transformation = lambda im: hog(im, block_norm='L2-Hys')
# chemins vers les images et les labels
input_images = 'project_train/train'
input_labels = 'project_train/label.txt'
# emplacement de sauvegarde du classifieur
output_classifier = 'trained_classifiers/forest_n100_hog_kfold.bin'

# ========================= Entrainement du classifieur ===============

start = time()
print("Chargement des donnees")
images = api.load_images(input_images)
labels = api.load_labels(input_labels)
print("Chargement termine en ", time() - start)

# On crée l'entraineur
trainer = FaceDetectorTrainer(
        windows_size=(hauteur, largeur),
        windows_step=pas,
        resizing_factors=facteurs,
        transform_function=transformation,
        classifier=cls
)

start = time()
print("Préparation des vecteurs d'apprentissage")
trainer.prepare_training_vectors(images, labels)
print("Prep terminee en ", time() - start)
# environ une dizaine de minutes sous W10 avec un i7 7700HQ

start = time()
print("Apprentisage")
perf = trainer.train_classifier_k_fold()
print("Apprentissage termine en ", time() - start)
print("Err =", perf)

# On sauvegarde le classifieur créé
trainer.dump_classifier(output_classifier)
